import React, { useState, } from 'react';
import Logo from '../../img/logo-black-text@3x.png';
import Add from '../../img/e-add@3x.png';
import PadLock from '../../img/padlock-unlocked@3x.png';
import Zoom from '../../img/zoom@3x.png';
import Home from '../../img/home@3x.png';
import ArrowDown from '../../img/small-down@3x.png'

import './topnav.css'

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';


const ExperienceTopNav = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <div className="nav-bar-top-half">
        <Navbar color="white" light expand="md">
          <NavbarBrand href="https://villavox.com/">
            <img className="villa-vox-logo" src={Logo} alt="VillaVox"></img>
          </NavbarBrand>
          
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar></Nav>
            
            <NavbarText>
              <img className="add" src={Add} alt="Add"></img>
              <a className="share-your-story-text" href="#">
                Share your story<a className="vertical-line"></a>
              </a>
            </NavbarText>

            <NavbarText>
              <img className="pad-lock-unlocked" src={PadLock} alt="PadLock"></img>
              <a className="sign-in-text" href="#">
                Sign in
              </a>
            </NavbarText>
            
            <button type="button" className="join-button">Join</button>
          </Collapse>    
        </Navbar>
      </div>
      

      <div className="nav-bar-bottom-half">
        <Navbar color="white"  expand="md">
          <div className="search-wrapper">
            <img src={Zoom} alt="zoom"></img>
            <input className = "search-box" type="text" name="searchBox" placeholder="Queen Anne, Seattle"></input>
          </div>

          <UncontrolledDropdown className="drop-down-1" nav inNavbar>
              <DropdownToggle nav>
                <img className="home" src={Home} alt="home"></img>
                Any type
                <img className="arrow-down" src={ArrowDown} alt="arrow-down"></img>
              </DropdownToggle>

              <DropdownMenu >
                <DropdownItem className="drop-down-menu one">Any type</DropdownItem>
                <DropdownItem className="drop-down-menu two">House</DropdownItem>
                <DropdownItem className="drop-down-menu three">Townhouse</DropdownItem>
                <DropdownItem className="drop-down-menu four">Condo</DropdownItem>
              </DropdownMenu>
          </UncontrolledDropdown>
        </Navbar>
      </div>
    </div>
  );
}

export default ExperienceTopNav;
